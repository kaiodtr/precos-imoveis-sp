import pickle

from flask import Flask, jsonify, request
from flask_cors import CORS

from numpy import expm1, nan
from pandas import DataFrame

app = Flask(__name__)
CORS(app)

# Carregando o modelo
model = pickle.load(open('model.sav', 'rb'))


@app.route('/health_check')
def health_check():
    return "Alive"


@app.route('/predict_price', methods=['POST'])
def predict_price():
    request_data = request.get_json()

    # Construindo DataFrame no formato necessário para entrada no modelo
    df_input = DataFrame([
        {
            'condo': request_data.get('condo', nan),
            'size': request_data.get('size', nan),
            'rooms': request_data.get('rooms', nan),
            'toilets': request_data.get('toilets', nan),
            'suites': request_data.get('suites', nan),
            'parking': request_data.get('parking', nan),
            'elevator': request_data.get('elevator', nan),
            'furnished': request_data.get('furnished', nan),
            'swimming_pool': request_data.get('swimming_pool', nan),
            'new': request_data.get('new', nan),
            'district': request_data.get('district', None),
        }
    ])

    # Obtendo a predição de preço usando o modelo desenvolvido
    # Note que é necessário fazer uma transformação (exponencial) na saída do
    # modelo
    predicted_price = expm1(model.predict(df_input)[0])
    
    # Retornando o preço previsto (arredondado)
    return jsonify(
        {'predicted_price': round(predicted_price, -2)}
    )
