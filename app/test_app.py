import json
from unittest import TestCase
from app import app


class TestAPI(TestCase):
    def setUp(self) -> None:
        self.app_client = app.test_client()
    
    def test_health_check(self):
        response = self.app_client.get('/health_check')
        assert response.data.decode('utf-8') == 'Alive'

    def test_prediction(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_size(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 836300.00


    def test_prediction_missing_condo(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 975400.00


    def test_prediction_missing_rooms(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 953800.00


    def test_prediction_missing_suites(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_toilets(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_parking(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 844100.00


    def test_prediction_missing_elevator(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 989300.00


    def test_prediction_missing_furnished(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'swimming_pool': 1,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_swimming_pool(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'new': 0,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_new(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'district': 'Vila Madalena',
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 973400.00


    def test_prediction_missing_district(self):
        response = self.app_client.post(
            '/predict_price',
            json={
                'size': 74,
                'condo': 1000,
                'rooms': 1,
                'suites': 1,
                'toilets': 2,
                'parking': 2,
                'elevator': 1,
                'furnished': 0,
                'swimming_pool': 1,
                'new': 0,
            }
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 902400.00


    def test_prediction_missing_all(self):
        response = self.app_client.post(
            '/predict_price',
            json={}
        )

        predicted_price = json.loads(response.data).get('predicted_price')

        assert predicted_price == 670900.00
