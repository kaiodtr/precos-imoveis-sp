# Predição de Preços de Imóveis em São Paulo

### Aluno: Kaio Douglas Teófilo Rocha

---

Nesta atividade, tivemos como objetivo realizar a migração de uma solução de machine learning desenvolvida em um notebook Jupyter para uma aplicação Web, de forma que fosse criada uma API que retorna a previsão de um modelo conforme recebe requisições do tipo POST contendo os dados de entrada do modelo. A API foi criada usando o framework Flask e serve um modelo de previsão de preços de venda de imóveis na cidade de São Paulo.

A arquitetura da aplicação conta com um servidor WSGI [gunicorn](https://gunicorn.org/) em substituição ao servidor padrão do Flask. Além disso, também há um servidor HTTP convencional [nginx](https://nginx.org/en/), de forma que seja possível disponibilizar tanto a API do modelo, quanto uma página Web estática onde o usuário pode inserir os dados a respeito de um imóvel e receber uma previsão de seu preço de venda.

Para implantar essa arquitetura, foi considerada uma solução usando um container [Docker](https://www.docker.com/) único, contendo a API do modelo (juntamente com o servidor WSGI) e o servidor nginx. Usando uma pipeline CI/CD desenvolvida no GitLab, a imagem do container é construída e publicada no [Docker Hub](https://hub.docker.com/). Além disso, o deploy da aplicação é feito na plataforma [Render](https://render.com/), que usa a [imagem](https://hub.docker.com/r/kaiodtr/api-precos-imoveis-sp) publicada no Docker Hub.

Todo o desenvolvimento desse projeto foi feito utilizando a versão `3.12.1` do Python.


## Testando a Aplicação

Para testar a aplicação, basta acessar o site https://api-precos-imoveis-sp.onrender.com/, onde há um formulário onde o usuário pode preencher os dados sobre um apartamento de interesse e, ao clicar no botão `Enviar`, receber uma previsão de preço de venda gerada pelo modelo.

> OBS.: A página pode demorar cerca de um minuto para carregar.


## Organização do Pacote

Este pacote contém este arquivo `README.md` e a pasta `app`, onde podem ser encontrados os arquivos:

- `app.py`
    - Código da API desenvolvida usando o framework Flask.

- `test_app.py`
    - Código dos testes unitários da aplicação.

- `model.sav`
    - Modelo de machine learning desenvolvido para realizar as predições de preços de imóveis.

- `requirements.txt`
    - Listagem de requisitos de pacotes Python necessários para execução da aplicação.

- `default`
    - Arquivo de configuração do servidor nginx.

- `index.html`
    - Página HTML com um formulário onde os usuários podem inserir dados sobre um imóvel específico e receber uma previsão de preço obtida a partir de uma requisição à API do modelo.

- `index_local.html`
    - Mesma página HTML de consumo da API, mas para execução local.

- `entrypoint.sh`
    - Script para execução da aplicação e do servidor nginx a partir de um container.

- `Dockerfile`
    - Dockerfile para criação da imagem do container da aplicação.

- `Dockerfile-local`
    - Dockerfile para criação da imagem do container da aplicação para execução local.


## Sobre o Modelo

O modelo desenvolvido teve como objetivo a realização de previsão de preços de imóveis na cidade de São Paulo.

Para desenvolver o modelo, foi utilizado um conjunto de dados disponibilizado no [Kaggle](https://www.kaggle.com/datasets/argonalyst/sao-paulo-real-estate-sale-rent-april-2019).

O conjunto de dados contém cerca de 13.000 apartamentos que estavam à venda ou disponíveis para aluguel na cidade de São Paulo em abril de 2019. Os dados têm origem em diversas fontes, principalmente de sites especializados em imóveis.

No projeto do modelo, foram considerados apenas os imóveis à venda.

O modelo final consiste em modelo de regressão linear simples, contando também com uma etapa de pré-processamento de dados.


## Instruções para Executar a Aplicação Localmente

Primeiramente, certifique-se de que você tem o [Docker](https://www.docker.com/) instalado em sua máquina.

Usando um terminal (ex: bash), navegue até o diretório `app` onde o arquivo `Dockerfile-local` está. Para criar a imagem do container da aplicação, execute o seguinte comando:

```bash
$ docker build -f Dockerfile-local -t api-precos-imoveis-sp .
```
Com isso, ao executar o comando `docker image ls`, a imagens criada (`api-precos-imoveis-sp`) deve ser exibida.

Agora, vamos criar o container da aplicação. Para isso, execute o comando:

```bash
$ docker run -d --rm -p 80:80 --name api-precos-imoveis-sp-container api-precos-imoveis-sp
```

Note que é preciso mapear a porta 80 do container do para que possamos acessá-lo localmente.

Utilize o comando `docker ps` para se certificar de que o container foi iniciado.

Para encerrar a execução do container, use o seguinte comando:

```bash
$ docker container stop api-precos-imoveis-sp-container
```


## Consumindo a Aplicação Localmente

Para consumir a aplicação (assumindo que o container esteja em execução, conforme instruções acima), podemos simplesmente abrir um navegador e apontar para o endereço `http://127.0.0.1`.

Como mencionado anteriormente, deve ser exibida uma página com um formulário onde o usuário pode inserir os dados referentes a um apartamento para o qual deseja obter uma previsão de preço.

Após preencher todos os campos, basta clicar no botão `Enviar`, que fará uma requisição à API. Caso nenhum erro ocorra, o preço previsto deve aparecer no campo correspondente.
